package com.knockar.navigation;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;

import com.knockar.R;

@SuppressLint({ "NewApi", "InflateParams" }) 
public class KnockarServiceFragment extends Fragment {
	
	//protected static final String LAYOUT_INFLATER_SERVICE = null;
	private Button serviceBttn;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        serviceBttn = (Button) rootView.findViewById(R.id.service);
        return rootView;
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		serviceBttn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				/*View popupView = getActivity().getLayoutInflater().inflate(R.layout.popupnew, null);  
				final PopupWindow popupWindow = new PopupWindow(
						popupView, 
						300,  
						470);*/  
				/*LayoutInflater layoutInflater 
			     = (LayoutInflater)getActivity()
			      .getSystemService(LAYOUT_INFLATER_SERVICE);  
			    View popupView = layoutInflater.inflate(R.layout.popupnew, null);  
			             final PopupWindow popupWindow = new PopupWindow(
			               popupView, 
			               LayoutParams.WRAP_CONTENT,  
			                     LayoutParams.WRAP_CONTENT);*/ 

			/*	
				LayoutInflater layoutInflater 
			     = (LayoutInflater)getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);  
			    View popupView1 = layoutInflater.inflate(R.layout.registration, null);  
			             final PopupWindow popupWindow1 = new PopupWindow(
			               popupView1, 
			               LayoutParams.FILL_PARENT,  
			                     LayoutParams.FILL_PARENT); 
			             
			             
			*/
				
				 LayoutInflater inflater = (LayoutInflater) getActivity()
		                    .getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
		            //Inflate the view from a predefined XML layout
		            View layout = inflater.inflate(R.layout.popupnew,
		                   (ViewGroup) getActivity().findViewById(R.id.jain));
		            // create a 300px width and 470px height PopupWindow
		           PopupWindow pw = new PopupWindow(layout, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, false);
		            // display the popup in the center
		            pw.showAtLocation(layout, Gravity.CENTER, 0, 0);
		            
				}
		});
	}
}
